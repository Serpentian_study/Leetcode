/* Ransom Note
 *
 * Given two strings ransomNote and magazine, return true if ransomNote
 * can be constructed from magazine and false otherwise.
 *
 * Each letter in magazine can only be used once in ransomNote.
 */

#include <algorithm>
#include <array>
#include <string>

class Solution {
public:
  bool canConstruct(std::string ransomNote, std::string magazine) {
    std::array<size_t, 26> arr1 = {0}, arr2 = {0};

    std::for_each(ransomNote.begin(), ransomNote.end(),
                  [&arr1](const char &ch) { ++arr1[ch - 'a']; });
    std::for_each(magazine.begin(), magazine.end(),
                  [&arr2](const char &ch) { ++arr2[ch - 'a']; });

    for (size_t i = 0; i < arr1.size(); ++i) {
      if (arr1[i] > arr2[i]) {
        return false;
      }
    }

    return true;
  }
};
