/* Valid Anagram
 *
 * Given two strings s and t, return true if t is an anagram of s, and false
 * otherwise.
 *
 * An Anagram is a word or phrase formed by rearranging the letters of a
 * different word or phrase, typically using all the original letters exactly
 * once.
 */

#include <algorithm>
#include <array>
#include <string>

class Solution {
public:
  bool isAnagram(std::string s, std::string t) {
    if (s.size() != t.size()) {
      return false;
    }

    std::array<size_t, 26> arr1 = {0}, arr2 = {0};
    std::for_each(s.begin(), s.end(),
                  [&arr1](const char &ch) { ++arr1[ch - 'a']; });
    std::for_each(t.begin(), t.end(),
                  [&arr2](const char &ch) { ++arr2[ch - 'a']; });

    return arr1 == arr2;
  }
};
