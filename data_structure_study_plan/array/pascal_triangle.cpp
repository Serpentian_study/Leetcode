/* Pascal's Triangle
 *
 * Given an integer numRows, return the first numRows of Pascal's triangle.
 * In Pascal's triangle, each number is the sum of the two numbers directly
 * above it
 *
 * Constraints:
 *  - 1 <= numRows <= 30
 */

#include <vector>
#include <cstdlib>
#include <iostream>

class Solution {
public:
    std::vector<std::vector<int>> generate(int numRows) {
        std::vector<std::vector<int>> answer (numRows);
        for (int i = 1; i <= numRows; ++i) {
            std::vector<int> row (i);
            row[0] = 1, row[i - 1] = 1;
            if (i > 2) {
                for (int j = 1; j < i - 1; ++j) {
                    row[j] = answer[i - 2][j - 1] + answer[i - 2][j];
                }
            }

            answer[i - 1] = std::move(row);
        }

        return answer;
    }
};
