/* Best time to buy and sell stock
 * You are given an array prices where prices[i] is the price of
 * a given stock on the ith day.
 *
 * You want to maximize your profit by choosing a single day to
 * buy one stock and choosing a different day in the future to
 * sell that stock.
 *
 * Return the maximum profit you can achieve from this transaction.
 * If you cannot achieve any profit, return 0.
 */

#include <cstdlib>
#include <vector>

class Solution {
public:
  int maxProfit(std::vector<int> &prices) {
    int min_price = prices[0], max_price = prices[0], max_profit = 0;

    for (size_t i = 1; i < prices.size(); ++i) {
      if (prices[i] > max_price) {
        max_price = prices[i];
      } else if (prices[i] < min_price) {
        int temp_profit = max_price - min_price;
        max_profit = temp_profit > max_profit ? temp_profit : max_profit;
        min_price = prices[i];
        max_price = prices[i];
      }
    }

    int temp_profit = max_price - min_price;
    return temp_profit > max_profit ? temp_profit : max_profit;
  }
};
