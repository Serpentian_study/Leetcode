/* Contains Duplicate
 *
 * Given an integer array nums, return true if any
 * value appears at least twice in the array, and
 * return false if every element is distinct.
 */

#include <cstdlib>
#include <functional>
#include <iostream>
#include <unordered_set>
#include <vector>

/*
 * First Solution
 */

// Time Limit Exceed
class Solution1 {
  struct Object {
    int val;
    enum { EMPTY, BUSY } state;
  };

  std::vector<Object> bucket_;

  bool insert(int val) {
    size_t hash = std::hash<int>{}(val);
    // linear probing
    for (size_t i = 0, idx = hash % bucket_.size(); i < bucket_.size();
         ++i, idx = (hash + i) % bucket_.size()) {

      if (bucket_[idx].state == Object::EMPTY) {
        bucket_[idx] = {val, Object::BUSY};
        return true;

      } else if (bucket_[idx].state == Object::BUSY &&
                 bucket_[idx].val == val) {
        return false;
      }
    }

    return false;
  }

public:
  bool containsDuplicate(std::vector<int> &nums) {
    bucket_.resize(nums.size(), {0, Object::EMPTY});
    for (const auto &x : nums) {
      // if can't insert duplicate
      if (!insert(x)) {
        return true;
      }
    }

    return false;
  }
};

/*
 * Second one
 */

// Pretty the same one
class Solution {
public:
  bool containsDuplicate(std::vector<int> &nums) {
    std::unordered_set<int> set;
    for (const auto &x : nums) {
      if (!set.insert(x).second) {
        return true;
      }
    }

    return false;
  }
};
