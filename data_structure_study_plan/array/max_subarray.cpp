/* Maximum subarray
 *
 * Given an integer array nums, find the contiguous
 * subarray (containing at least one number) which has
 * the largest sum and return its sum.
 *
 * A subarray is a contiguous part of an array.
 */

#include <cstdlib>
#include <vector>

class Solution {
public:
  int maxSubArray(std::vector<int> &nums) {
    int global_max_sum = nums[0], current_max_sum = nums[0];
    for (size_t i = 1; i < nums.size(); ++i) {
      current_max_sum = std::max(current_max_sum + nums[i], nums[i]);
      global_max_sum = std::max(global_max_sum, current_max_sum);
    }

    return global_max_sum;
  }
};
