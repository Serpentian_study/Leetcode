/* Search a 2D Matrix
 *
 * Write an efficient algorithm that searches for a value target in an m x n
 * integer matrix matrix. This matrix has the following properties:
 *
 * Integers in each row are sorted from left to right.
 * The first integer of each row is greater than the last integer
 * of the previous row.
 */

#include <cstdlib>
#include <vector>

class Solution {
public:
  // treating matrix as sorted array
  bool searchMatrix(std::vector<std::vector<int>> &matrix, int target) {
    size_t m = matrix.size(), n = matrix[0].size(), arr_size = m * n;
    int64_t left_bound = 0, right_bound = arr_size - 1;

    while (left_bound <= right_bound) {
      size_t mid = left_bound + (right_bound - left_bound) / 2;
      int value = matrix[mid / n][mid % n];
      if (value == target) {
        return true;
      } else if (value > target) {
        right_bound = mid - 1;
      } else {
        left_bound = mid + 1;
      }
    }

    return false;
  }
};
