/* Reshape the matrix
 *
 * In MATLAB, there is a handy function called reshape which can reshape
 * an m x n matrix into a new one with a different size r x c keeping its
 * original data.
 *
 * You are given an m x n matrix mat and two integers r and c representing
 * the number of rows and the number of columns of the wanted reshaped matrix.
 *
 * The reshaped matrix should be filled with all the elements of the original
 * matrix in the same row-traversing order as they were.
 *
 * If the reshape operation with given parameters is possible and legal,
 * output the new reshaped matrix; Otherwise, output the original matrix.
 */

#include <cstdlib>
#include <vector>

class Solution {
public:
  std::vector<std::vector<int>>
  matrixReshape(std::vector<std::vector<int>> &mat, int r, int c) {
    size_t m = mat.size(), n = mat[0].size();

    if (m * n != r * c) {
      return mat;
    }

    std::vector<int> expanded_mat(m * n);
    for (size_t i = 0; i < m; ++i) {
      std::copy(mat[i].begin(), mat[i].end(), expanded_mat.begin() + n * i);
    }

    std::vector<std::vector<int>> answer(r);
    for (size_t i = 0; i < r; ++i) {
        auto begin_iter = expanded_mat.begin() + c * i;
        answer[i] = std::vector<int> (begin_iter, begin_iter + c);
    }

    return answer;
  }
};
