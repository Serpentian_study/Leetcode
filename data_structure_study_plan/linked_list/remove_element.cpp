/* Remove Linked List Elements
 *
 * Given the head of a linked list and an integer val, remove all the
 * nodes of the linked list that has Node.val == val, and return the new head.
 */

struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class Solution {
public:
    // Using recursion just for training
    ListNode* removeElements(ListNode* head, int val) {
        if (!head) {
            return head;
        }

        head->next = removeElements(head->next, val);

        if (head->val == val) {
            auto temp (head);
            head = head->next;
            delete temp;
        }

        return head;
    }
};
