/* Linked List Cycle
 *
 * Given head, the head of a linked list, determine if the linked list has
 * a cycle in it.
 */

struct ListNode {
  int val;
  ListNode *next;
  ListNode(int x) : val(x), next(nullptr) {}
};

class Solution {
public:
  // Floyd’s Cycle Finding Algorithm
  bool hasCycle(ListNode *head) {
    if (!head)
      return false;

    ListNode *fast = head, *slow = head;

    while (slow && fast && fast->next) {
      fast = fast->next->next;
      slow = slow->next;
      if (fast == slow) {
        return true;
      }
    }

    return false;
  }
};
