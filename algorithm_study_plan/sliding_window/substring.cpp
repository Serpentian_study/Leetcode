/* Longest Substring Without Repeating Characters
 *
 * Given a string s, find the length of the longest substring
 * without repeating characters.
 */

#include <string>
#include <unordered_map>

class Solution {
public:
  int lengthOfLongestSubstring(std::string s) {
    typedef std::unordered_map<char, std::string::iterator> HashTable;

    HashTable hash_table;
    std::pair<HashTable::iterator, bool> last_value;
    auto begin = s.begin(), end = s.begin();
    int length(0);

    while (end != s.end()) {

      // filling hash table
      while (end != s.end() &&
             (last_value = hash_table.insert({*end, end})).second) {
        ++end;
      }

      // updating length
      int current_length = end - begin;
      length = current_length > length ? current_length : length;

      if (!last_value.second) {
        // updating start idx
        auto prev_begin = begin;
        begin = last_value.first->second + 1;

        // cleaning hash_table (... , first_occurance_of_duplicate)
        auto temp = begin - 1;
        while (temp >= prev_begin && hash_table.erase(*temp)) {
          --temp;
        }
      }
    }

    return length;
  }
};
