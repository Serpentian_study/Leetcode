/* Binary search
 *
 * Given an array of integers nums which is sorted in ascending order,
 * and an integer target, write a function to search target in nums.
 * If target exists, then return its index. Otherwise, return -1.
 *
 * You must write an algorithm with O(log n) runtime complexity.
 */

#include <iostream>
#include <vector>

class Solution {
public:
  // avoid recursion as far as possible
  // why is it return int, when we wanna size_t?!?
  int search(std::vector<int> &nums, int target) {
    int left_bound = 0, right_bound = nums.size() - 1;

    while (left_bound <= right_bound) {
      int mid = left_bound + (right_bound - left_bound) / 2;

      if (nums[mid] > target) {
        right_bound = mid - 1;
      } else if (nums[mid] < target) {
        left_bound = mid + 1;
      } else {
        return mid;
      }
    }

    return -1;
  }
};
