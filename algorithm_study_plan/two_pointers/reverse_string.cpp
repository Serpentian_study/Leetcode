/* Reverse String
 *
 * Write a function that reverses a string. The input string is
 * given as an array of characters s.
 *
 * You must do this by modifying the input array in-place with O(1)
 * extra memory.
 */

#include <cstdlib>
#include <vector>

class Solution {
public:
  void reverseString(std::vector<char> &s) {
    for (auto begin = s.begin(), end = --s.end(); begin < end; ++begin, --end) {
      std::swap(*begin, *end);
    }
  }
};
