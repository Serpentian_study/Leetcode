/* Middle of the Linked List
 *
 * Given the head of a singly linked list, return the middle node
 * of the linked list.
 *
 * If there are two middle nodes, return the second middle node.
 */

#include <cstdlib>

struct ListNode {
  int val;
  ListNode *next;
  ListNode() : val(0), next(nullptr) {}
  ListNode(int x) : val(x), next(nullptr) {}
  ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class Solution {
public:
  // Some kind of brute force
  // Regardless chosen algorithm: T = O(N), S = O(1)
  ListNode *middleNode(ListNode *head) {
    if (!head->next) {
      return head;
    }

    if (!head->next->next) {
        return head->next;
    }

    ListNode *last = head->next->next, *middle = head->next;
    size_t size = 3, middle_idx = 1;
    while (last->next) {
      last = last->next;
      ++size;
      for (; middle_idx < size / 2; ++middle_idx) {
        middle = middle->next;
      }
    }

    return middle;
  }

  ListNode *middleNode2(ListNode *head) {
      ListNode* fast = head, *slow = head;
      while (fast && fast->next) {
          slow = slow->next;
          fast = fast->next->next;
      }

      return slow;
  }

};
