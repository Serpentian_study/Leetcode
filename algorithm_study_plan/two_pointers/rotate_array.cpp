/* Rotate Array
 *
 * Given an array, rotate the array to the right by k steps,
 * where k is non-negative.
 */

#include <algorithm>
#include <vector>

class Solution {
public:
  void rotate(std::vector<int> &nums, int k) {
    std::vector<int>::iterator break_point = nums.end() - (k % nums.size());
    std::reverse(nums.begin(), break_point);
    std::reverse(break_point, nums.end());
    std::reverse(nums.begin(), nums.end());
  }
};
