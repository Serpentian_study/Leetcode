#!/usr/bin/env python3

# Given an integer array nums sorted in non-decreasing order,
# return an array of the squares of each number sorted in
# non-decreasing order.


class Solution(object):
    # O(n * log(n)) - not the great one
    def sortedSquares(self, nums: list[int]) -> list[int]:
        return sorted(list(x ** 2 for x in nums))

    # O(n) - two-pointers method
    # in python it works much slower rather than the first method
    # However, it's complexity is musch better
    def optimizedSortedSquares(self, nums: list[int]) -> list[int]:
        nums = list(abs(x) for x in nums)
        end = len(nums) - 1
        begin = 0

        answer = []
        while end >= begin:
            if nums[end] > nums[begin]:
                answer.insert(0, nums[end] ** 2)
                end -= 1
            else:
                answer.insert(0, nums[begin] ** 2)
                begin += 1

        return answer


print(Solution().optimizedSortedSquares([-36, 1, 5]))
